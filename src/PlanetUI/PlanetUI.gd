extends Control

var planet : Planet = null
var player : Player = null

signal on_buy
signal on_planet_ui_return

func _ready() -> void:
	pass
	
func reset(plr : Player, plnt : Planet) -> void:
	player = plr
	planet = plnt
	$Panel/VBoxContainer/PlanetNameLabel.text = planet.get_planet_name()
	
	for child in $Panel/VBoxContainer/ResourcesVBoxContainer.get_children():
		$Panel/VBoxContainer/ResourcesVBoxContainer.remove_child(child)
	
	var resources : Dictionary = planet.resources
	
	for res in resources.keys():
		var stock = resources[res]
		
		var label : Label = Label.new()
		label.text = "- " + res.resource_name + " : " + str(stock.cost) + "$" + " x" + str(stock.amount)
		
		var button : Button = Button.new()
		button.text = "Buy"

		button.connect("pressed", self, "_buy_button_on_pressed", [res])
		
		var hbox : HBoxContainer = HBoxContainer.new()
		hbox.add_child(label)
		hbox.add_child(button)
		$Panel/VBoxContainer/ResourcesVBoxContainer.add_child(hbox)
	
func _buy_button_on_pressed(res) -> void:
	var resource_available : bool = planet.resources[res].amount - 1 >= 0
	var player_has_money : bool = player.get_money().get_amount() - planet.resources[res].cost >= 0
	var player_has_capacity : bool = player.get_inventory().capacity + 1 <= player.get_inventory().MAX_CAPACITY
	
	if resource_available and player_has_money and player_has_capacity:
		player.get_money().add(-planet.resources[res].cost)
		planet.resources[res].amount -= 1
		
		for r in $SpaceResource.get_children():
			if r.resource_name == res.resource_name:
				player.add_resource(r)
		reset(player, planet)
		emit_signal("on_buy")

func _on_sell(resource, planet) -> void:
	for r in planet.resources.keys():
		if r.resource_name == resource.resource_name:
			planet.resources[r].amount += 1
	reset(player, planet)

func _on_ReturnButton_pressed() -> void:
	emit_signal("on_planet_ui_return")
