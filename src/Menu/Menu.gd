extends Control

signal on_new_game
signal on_load_game

func _ready() -> void:
	var err = connect("on_load_game", LoadManager, "load_game")
	assert err == OK
	
	err = connect("on_new_game", LoadManager, "new_game")
	assert err == OK
	
	var file : File = File.new()
	$VBoxContainer/LoadGameButton.disabled = not file.file_exists(SaveManager.SAVE_PATH)
	
func _on_NewGameButton_pressed() -> void:
	emit_signal("on_new_game")

func _on_LoadGameButton_pressed() -> void:
	emit_signal("on_load_game")

func _on_QuitButton_pressed() -> void:
	get_tree().quit()
