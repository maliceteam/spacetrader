extends Node
class_name Sector

const MIN_N_PLANETS : int = 3
const MAX_N_PLANETS : int = 10

var i : int
var j : int

var row : String
var column : int
var sector_name : String = "Sector " + row + "-" + str(column)
var radius : float = 4096 * 4

var n_planets : int

func _ready() -> void:
	pass

func serialize() -> Dictionary:
	var dict : Dictionary = {}
	dict["Row"] = row
	dict["Column"] = column
	dict["SectorName"] = sector_name
	dict["Radius"] = radius
	dict["I"] = i
	dict["J"] = j
	dict["Planets"] = {}
	for planet in get_children():
		dict["Planets"][planet.planet_name] = planet.serialize()
	return dict

func unserialize(dict : Dictionary) -> void:
	row = dict["Row"]
	column = dict["Column"]
	sector_name = dict["SectorName"]
	radius = dict["Radius"]
	i = dict["I"]
	j = dict["J"]
	
	for key in dict["Planets"].keys():
		var planet : Planet = preload("res://Planet/Planet.tscn").instance()
		planet.unserialize(dict["Planets"][key])
		add_child(planet)
	

	
func set_localisation(r : String, c : int) -> void:
	row = r
	column = c
	i = row.to_ascii()[0] - 65
	j = column
	
	sector_name = "Sector " + row + "-" + str(column)

func generate(the_seed : int) -> void:
	if the_seed >= 0:
		seed(the_seed)
	else:
		randomize()
	
	n_planets = int(round(rand_range(MIN_N_PLANETS, MAX_N_PLANETS)))
	
	for i in range(0, n_planets):
		var planet_rsc = load("res://Planet/Planet.tscn")
		var planet : Planet = planet_rsc.instance()
		var id : int = round(rand_range(planet.MIN_ID, planet.MAX_ID))
		var name_id : int = randi()
		planet.set_id(id)
		planet.set_name_id(name_id)
		planet.set_planet_name("S"+row+str(column)+"P"+str(i+1) + " : " + NameGeneratorManager.generate_name(name_id))
		
		var pos = Vector2(rand_range(-radius/2, radius/2), rand_range(-radius/2, radius/2))
		while planet_at_position(pos):
			pos = Vector2(rand_range(-radius/2, radius/2), rand_range(-radius/2, radius/2))
		
		planet.set_position(pos)
		planet.generate_resources()
		
		add_child(planet)

func planet_at_position(pos : Vector2) -> bool:
	for node in get_children():
		if node is Planet:
			var planet : Planet = node
			var collide = (pos - planet.get_position()).length() < planet.radius
			if collide:
				return true
	return false
	
func planet_nearest(pos : Vector2) -> Planet:
	var planet : Planet = null
	
	for node in get_children():
		if node is Planet:
			if planet == null or (node.get_position() - pos).length_squared() < (planet.get_position() - pos).length_squared():
				planet = node
				
	return planet