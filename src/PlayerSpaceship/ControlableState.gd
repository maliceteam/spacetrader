extends Node

var spaceship : Spaceship = null
const STATE_NAME : String = "Moving"

func _ready() -> void:
	pass

func process(delta) -> void:
	if Input.is_action_pressed("player_left"):
		spaceship.move_angle(-spaceship.ANGLE_VEL * delta)
	if Input.is_action_pressed("player_right"):
		spaceship.move_angle(spaceship.ANGLE_VEL * delta)
	if Input.is_action_pressed("player_speed"):
		if spaceship.get_fuel_amount() > 0.0:
			spaceship.vel = spaceship.FAST_VEL
			spaceship.add_fuel(spaceship.CONSO_FUEL * delta)
		else:
			spaceship.vel = spaceship.SLOW_VEL
	else:
		spaceship.vel = spaceship.SLOW_VEL

func physics_process(delta) -> void:
	pass