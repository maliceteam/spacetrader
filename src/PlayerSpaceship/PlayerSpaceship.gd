class_name PlayerSpaceship
extends Node

onready var spaceship : KinematicBody2D = $Spaceship
onready var state : Node = $States/ControlableState
const ORBIT_DISTANCE : float = 512.0

func _ready() -> void:
	state.spaceship = $Spaceship
	
func _process(delta) -> void:
	state.process(delta)

func _physics_process(delta) -> void:
	state.physics_process(delta)

##########
# States #
##########

func get_state() -> String:
	return state.name

func get_state_name() -> String:
	return state.STATE_NAME
	
func controlable_state() -> void:
	state = $States/ControlableState
	state.spaceship = $Spaceship

func fast_travel_state() -> void:
	spaceship.vel = spaceship.FAST_VEL
	spaceship.add_fuel(spaceship.FAST_TRAVEL_CONSO_FUEL)
	state = $States/FastTravelState
	state.spaceship = $Spaceship

func orbit_state(planet : Planet) -> void:
	if (planet.get_position() - spaceship.get_position()).length() <= ORBIT_DISTANCE:
		state = $States/OrbitState
		state.should_orbit = false
		state.spaceship = $Spaceship
		state.planet = planet

#################
# Getter/Setter #
#################

func get_position() -> Vector2:
	return $Spaceship.global_position
	
func set_position(pos : Vector2) -> void:
	$Spaceship.global_position = pos

func get_dir() -> Vector2:
	return $Spaceship.dir

func get_vel() -> Vector2:
	return $Spaceship.vel