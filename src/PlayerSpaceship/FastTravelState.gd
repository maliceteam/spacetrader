extends Node

var spaceship : Spaceship = null
const STATE_NAME : String = "Fast Traveling"

func _ready() -> void:
	pass

func process(delta) -> void:
	spaceship.vel = spaceship.FAST_TRAVEL_VEL
	
func physics_process(delta) -> void:
	pass