extends Node

var spaceship : PlayerSpaceship

func _init(playerspaceship) -> void:
	spaceship = playerspaceship
	
func _ready() -> void:
	pass

func _process(delta) -> void:
	if Input.is_action_pressed("player_left"):
			spaceship.move_angle(-spaceship.ANGLE_VEL * delta)
	if Input.is_action_pressed("player_right"):
		spaceship.move_angle(spaceship.ANGLE_VEL * delta)
	if Input.is_action_pressed("player_speed"):
		spaceship.vel = spaceship.MAX_VEL

func _physics_process(delta) -> void:
	pass