extends Node

var spaceship : Spaceship = null
var planet : Planet = null
const STATE_NAME : String = "Orbiting"

const ORBIT_DISTANCE : float = -256.0
var should_orbit : bool = false

func _ready() -> void:
	pass

func process(delta) -> void:
	spaceship.vel = spaceship.ORBIT_VEL
	var to_planet : Vector2 = planet.get_position() - spaceship.get_position()
	var orbit_dist = ORBIT_DISTANCE + planet.radius
	var dist : float = to_planet.length()
	var delta_dist : float = 16.0
	
	if abs(orbit_dist - dist) < delta_dist:
		should_orbit = true
	
	if should_orbit:
		process_orbit()
	else:
		process_arrive()
	
func physics_process(delta) -> void:
	pass
	
func process_arrive() -> void:
	var to_planet : Vector2 = planet.get_position() - spaceship.get_position()
	var orbit_dist : float = ORBIT_DISTANCE + planet.radius
	var dist : float = to_planet.length()
	
	spaceship.angle = to_planet.angle() + PI/2
	
	if dist < orbit_dist:
		spaceship.angle += PI
	
func process_orbit() -> void:
	var angle2planet : float = spaceship.get_position().angle_to_point(planet.get_position())
	spaceship.angle = angle2planet
	