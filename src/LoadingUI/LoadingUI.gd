extends Control

var universe : Node

func _ready():
	call_deferred("load_universe")
	print("kazog")
	pass

func load_universe() -> void:
	universe = preload("res://Universe/Universe.tscn").instance()
	
	universe.sector_generate()
	universe.sector_change(0, 0)
	
	LoadManager.change_scene(universe)

func _process(delta):
	$Sprite.rotate(delta * 2)
	