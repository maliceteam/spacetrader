extends Control

export var time : float = 1.0
var timer : float = 0.0
var planet : Planet = null
signal scan_finished

func _ready() -> void:
	pass

func reset() -> void:
	timer = 0.0
	$Panel/VSplitContainer/FinishButton.visible = false
	
func _process(delta) -> void:
	timer += delta
	
	if timer > time:
		timer = time
		$Panel/VSplitContainer/FinishButton.visible = true
		if Input.is_action_just_pressed("scan"):
			finish()
	
	var bar = $ Panel/VSplitContainer/ProgressBar
	bar.value =  bar.max_value * timer/time

func _on_FinishButton_pressed() -> void:
	finish()

func finish() -> void:
	if planet != null:
		planet.is_scanned = true
		emit_signal("scan_finished")
