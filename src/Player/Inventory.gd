class_name Inventory
extends Node

class InventoryStock:
	var resource : SpaceResource
	var amount : int
	
var resources : Dictionary
const MAX_CAPACITY : int = 10
var capacity : int = 0

func _ready() -> void:
	pass

func serialize() -> Dictionary:
	var dict : Dictionary = {}
	dict["Resources"] = {}
	for r in resources:
		dict["Resources"][resources[r].resource.resource_name] = resources[r].amount
		
	dict["Capacity"] = capacity
	return dict 

func unserialize(dict : Dictionary) -> void:
	var dr : Dictionary = dict["Resources"]
	for key in dr.keys():
		for i in dr[key]:
			add_resource(space_resource_from_name(key))

func space_resource_from_name(spacename : String) -> SpaceResource:
	var sr_node : Node = preload("res://SpaceResource/SpaceResource.tscn").instance()
	for sr in sr_node.get_children():
		if sr.resource_name == spacename:
			return sr
	return null
	
func add_resource(res : SpaceResource) -> void:
	if resources.has(res.resource_name):
		resources[res.resource_name].amount += 1
	else:
		resources[res.resource_name] = InventoryStock.new()
		resources[res.resource_name].amount = 1
		resources[res.resource_name].resource = res 
	capacity += 1
	
func remove_resource(res : SpaceResource) -> void:
	if resources.has(res.resource_name):
		resources[res.resource_name].amount -= 1
	capacity -= 1

func resource_amount(res : SpaceResource) -> int:
	if resources.has(res.resource_name):
		return resources[res].amount
	else:
		return 0
