class_name Player
extends Node

signal on_fuel_reloaded

func _ready() -> void:
	pass

func serialize() -> Dictionary:
	return {
		"SpaceshipAngle" : $PlayerSpaceship/Spaceship.get_angle(),
		"SpaceshipPosX" : $PlayerSpaceship/Spaceship.get_position().x,
		"SpaceshipPosY" : $PlayerSpaceship/Spaceship.get_position().y,
		"SpaceshipFuel" : $PlayerSpaceship/Spaceship.fuel,
		"MoneyAmount" : $Money.amount,
		"Inventory" : $Inventory.serialize()
	}

func unserialize(dict : Dictionary) -> void:
	$PlayerSpaceship/Spaceship.set_angle(dict["SpaceshipAngle"])
	$PlayerSpaceship/Spaceship.set_position(Vector2(dict["SpaceshipPosX"], dict["SpaceshipPosY"]))
	$PlayerSpaceship/Spaceship.fuel = dict["SpaceshipFuel"]
	$Money.amount = dict["MoneyAmount"]
	$Inventory.unserialize(dict["Inventory"])
	
func get_player_spaceship() -> Node:
	return $PlayerSpaceship
	
func get_spaceship() -> Node:
	return $PlayerSpaceship/Spaceship
	
func get_money() -> Node:
	return $Money
	
func add_resource(res : SpaceResource) -> void:
	$Inventory.add_resource(res)

func get_inventory() -> Node:
	return $Inventory
	
func _on_reload_fuel() -> void:
	for res in $Inventory.resources.keys():
		if res == "Fuel" and $Inventory.resources[res].amount - 1 >= 0:
			$Inventory.resources[res].amount -= 1
			$Inventory.capacity -= 1
			$PlayerSpaceship/Spaceship.add_fuel($PlayerSpaceship/Spaceship.MAX_FUEL)
			emit_signal("on_fuel_reloaded")

func _on_sell(resource, planet) -> void:
	$Inventory.remove_resource(resource)
	$Money.add(planet.sell_prices[resource.resource_name])