class_name Money

extends Node

var amount : float = 100

func _ready() -> void:
	pass

func add(n : float) -> float:
	assert amount + n >= 0
	amount += n
	return amount
	
func get_amount() -> float:
	return amount