extends Node

const VOWEL : String = "aeiouy"
const CONSONANT : String = "zrtpqsdfghjklmwxcvbn"
const MIN_LENGTH : int = 2
const MAX_LENGTH : int = 4
var syllables : Array = []

func _ready():
	for v in VOWEL:
		for c in CONSONANT:
			syllables.push_back(c)
			syllables.push_back(v)


func get_syllable(index : int) -> Array:
	assert index < syllables.size()/2
	var i : int = 2 * index
	return [syllables[i], syllables[i + 1]]

func get_random_syllable(theseed : int) -> Array:
	if theseed >= 0:
		seed(theseed)
	else:
		randomize()
	return get_syllable(round(rand_range(0, syllables.size()/2 - 1)))

func generate_name(theseed : int) -> String:
	var name : String = ""
	var length : int = int(round(rand_range(MIN_LENGTH, MAX_LENGTH)))
	
	for i in range(0, length):
		var syllable : Array = get_random_syllable(theseed + i)
		name += syllable[0] + syllable[1]
	
	name = name.capitalize()
	
	return name