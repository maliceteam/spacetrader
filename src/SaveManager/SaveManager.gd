extends Node

const SAVE_PATH : String = "user://game.save"
const SAVE_PASS : String = "66adf5c35190af3b07aa4774884814db0c3ae73f"

func _ready() -> void:
	pass

func save(universe : Node, player : Node) -> void:
	var player_json : String = to_json(player.serialize())
	var universe_json : String = to_json(universe.serialize())
	
	var file : File = File.new()
	var err = file.open_encrypted_with_pass(SAVE_PATH, File.WRITE, SAVE_PASS)
	assert err == OK

	file.store_line(player_json)
	file.store_line(universe_json)
	file.close()