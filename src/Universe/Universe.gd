class_name Universe
extends Node2D

const N_SECTOR_W : int = 8
const N_SECTOR_H : int = N_SECTOR_W
const A_ASCII : int = 65

var sectors = []
var sector_current : Sector = null
var last_sector : Sector = null
var is_fast_travel : bool = false
var is_scanning : bool = false

onready var scan_ui = preload("res://PlanetScanUI/PlanetScanUI.tscn").instance()
onready var planet_ui = $PanelUI/Control/PlanetUI
onready var inventory_ui = $PanelUI/Control/InventoryUI


#########
# Ready #
#########

func _ready() -> void:
	inventory_ui.inventory = $Player/Inventory
	inventory_ui_hide()
	planet_ui_hide()
	pause_ui_hide()
	ready_connect()

func serialize() -> Dictionary:
	assert sector_current != null	
	var dict : Dictionary = {}
	dict["Sectors"] = {}
	dict["CurrentSectorI"] = sector_current.i
	dict["CurrentSectorJ"] = sector_current.j
	dict["Sectors"]["Sector"] = {}
	for sector in sectors:
		dict["Sectors"]["Sector"][sector.sector_name] = sector.serialize()
	return dict

func unserialize(dict : Dictionary) -> void:
	var dsector : Dictionary = dict["Sectors"]["Sector"]
	for key in dsector.keys():
		var sector : Sector = preload("res://Sector/Sector.tscn").instance()
		sector.unserialize(dsector[key])
		sectors.push_back(sector)
	sector_change(dict["CurrentSectorI"], dict["CurrentSectorJ"])
	
func ready_connect() -> void:
	var err = $PauseUI/PauseMenu.connect("on_save", SaveManager, "save", [self, $Player])
	assert err == OK
	
	planet_ui.connect("on_buy", inventory_ui, "_reset")
	planet_ui.connect("on_planet_ui_return", self, "planet_ui_hide")
	inventory_ui.connect("on_reload_fuel", $Player, "_on_reload_fuel")
	err = $Player.connect("on_fuel_reloaded", inventory_ui, "_reset")
	assert err == OK
	inventory_ui.connect("on_sell", $Player, "_on_sell")
	inventory_ui.connect("on_sell", planet_ui, "_on_sell")
	
###########
# Process #
###########

func _process(delta) -> void:
	var player_spaceship : PlayerSpaceship = $Player/PlayerSpaceship
	var spaceship : Spaceship = $Player/PlayerSpaceship/Spaceship
	
	if Input.is_action_pressed("ui_select") and player_spaceship.get_state() == "ControlableState" and spaceship.get_fuel_amount() + spaceship.FAST_TRAVEL_CONSO_FUEL >= 0:
		fast_travel_start()
		
	if Input.is_action_just_pressed("orbit"):
		if player_spaceship.get_state() == "ControlableState":
			var sector : Sector = sector_current
			assert sector != null
			var planet : Planet = sector.planet_nearest(player_spaceship.get_position())
			player_spaceship.orbit_state(planet)
		elif player_spaceship.get_state() == "OrbitState":
			planet_ui_hide()
			player_spaceship.controlable_state()
	
	if Input.is_action_just_pressed("scan") and player_spaceship.get_state() == "OrbitState":
		scan_ui_show()
	
	if Input.is_action_just_pressed("planet"):
		if planet_ui.visible:
			planet_ui_hide()
		else:
			if player_spaceship.get_state() == "OrbitState":
				planet_ui_show()
		
	if Input.is_action_just_pressed("inventory"):
		if inventory_ui.visible:
			inventory_ui_hide()
		else:
			inventory_ui_show()
	
	if Input.is_action_just_pressed("pause"):
		pause_ui_show()
		
	process_camera_position()
	
	if not is_fast_travel:
		process_sector_name()
		process_planet_distance()
	
	process_state_status()
	
	process_stars_move(delta)
	process_stars_update()
	
	process_pause_button()
	process_player_money()
	process_player_fuel()
	
func process_camera_position() -> void:
	$Camera2D.position = $Player.get_player_spaceship().get_position()

func process_sector_name() -> void:
	if sector_current != null:
		$UI/StatusBarUI/SectorLabel.text = sector_current.sector_name

func process_planet_distance() -> void:
	assert sector_get_current() != null
	var player_pos : Vector2 = $Player.get_player_spaceship().get_position()
	var planet : Planet = sector_get_current().planet_nearest(player_pos)
	var pname : String = planet.get_planet_name()
	var dist : float = (player_pos - planet.get_position()).length()
	$UI/StatusBarUI/RadarLabel.text = "Nearest planet : [" + pname + "] (" + str(dist) + ")"

func process_state_status() -> void:
	$UI/StatusBarUI/StateLabel.text = "[" + $Player/PlayerSpaceship.get_state_name() + "]"
	
func process_stars_move(delta) -> void:
	var player = $Player.get_player_spaceship()
	var stars = $Background/Stars
	
	for i in stars.stars.size():
		stars.stars[i] -= player.get_vel() * player.get_dir() * delta

func process_stars_update() -> void:
	var width : float = get_viewport_rect().size.x
	var height : float = get_viewport_rect().size.y
	var stars = $Background/Stars.stars
	
	for i in stars.size():
		if stars[i].x < 0:
			stars[i].x = width
		if stars[i].x > width:
			stars[i].x = 0
		if stars[i].y > height:
			stars[i].y = 0
		if stars[i].y < 0:
			stars[i].y = height

func process_pause_button() -> void:
	$UI/StatusBarUI/PauseButton.disabled = not $Player/PlayerSpaceship.get_state() == "ControlableState"

func process_player_money() -> void:
	$UI/StatusBarUI/MoneyLabel.text = str($Player.get_money().get_amount()) + "$"

func process_player_fuel() -> void:
	$UI/StatusBarUI/FuelProgressBar.value = $Player/PlayerSpaceship/Spaceship.fuel
###############
# Fast travel #
###############

func fast_travel_start() -> void:
	is_fast_travel = true
	$Player.get_player_spaceship().fast_travel_state()
	$Background/Stars.is_fast_travel = true
	$Background/Stars.fast_travel_dir = $Player.get_spaceship().dir
	$FastTravelTimer.start()
	
	if sector_current != null:
		last_sector = sector_current
		remove_child(sector_current)
		sector_current = null
	
func _fast_travel_stop() -> void:
	is_fast_travel = false
	$FastTravelTimer.stop()
	$Player.get_player_spaceship().controlable_state()
	$Background/Stars.is_fast_travel = false
	fast_travel_change_sector()

func fast_travel_change_sector() -> void:
	assert last_sector != null
	
	var dir : Vector2 = $Player.get_player_spaceship().get_dir()
	if abs(dir.x) > abs(dir.y):
		dir.y = 0
	else:
		dir.x = 0
	
	dir = dir.normalized()
	
	var sector_i : int = last_sector.i
	var sector_j : int = last_sector.j
	
	sector_i += int(dir.y)
	sector_j += int(dir.x)
	
	if sector_exists(sector_i, sector_j):
		sector_change(sector_i, sector_j)
	else:
		sector_change(last_sector.i, last_sector.j)
	
	$Player.get_player_spaceship().set_position(Vector2())

###########
# SECTORS #
###########

func sector_string_row(row : int) -> String:
	var pba : PoolByteArray = "".to_ascii()
	pba.append(A_ASCII + row)
	return pba.get_string_from_ascii()
	
func sector_generate() -> void:
	for r in range(0, 8):
		for c in range(0, 8):
			var row : String = sector_string_row(r)
			var column : int = c
			var sector_rsc : PackedScene = preload("res://Sector/Sector.tscn")
			var sector : Sector = sector_rsc.instance()
			
			sector.set_localisation(row, column)
			sector.generate(-1)
			sectors.append(sector)

func sector_exists(i : int, j : int) -> bool:
	return i >= 0 and i < N_SECTOR_H and j >= 0 and j < N_SECTOR_W

func sector_get(i : int, j : int) -> Sector:
	assert sector_exists(i, j)
	return sectors[i * N_SECTOR_W + j]

func sector_change(i : int, j : int) -> void:
	for c in get_child_count():
		if get_child(c) is Sector:
			remove_child( get_child(c) )
	
	add_child( sector_get(i, j) )
	sector_current = sector_get(i, j)
	
func sector_get_current() -> Sector:
	return sector_current
	
###############
# InventoryUI #
###############

func inventory_ui_show() -> void:
	inventory_ui.visible = true
	inventory_ui.reset_resources()
	inventory_ui.add_resources($Player/Inventory.resources)

func inventory_ui_hide() -> void:
	inventory_ui.visible = false
	
################
# ScanPlanetUI #
################

func scan_ui_show() -> void:
	assert sector_get_current() != null
	
	if not is_scanning:
		var sector : Sector = sector_get_current()
		var planet : Planet = sector.planet_nearest($Player/PlayerSpaceship.get_position())
		if not planet.is_scanned:
			is_scanning = true	
			scan_ui.planet = planet
			scan_ui.connect("scan_finished", self, "scan_ui_hide")
			scan_ui.reset()
			$UI.add_child(scan_ui)
	
func scan_ui_hide() -> void:
	is_scanning = false
	$UI.remove_child(scan_ui)
	
###################
# PlanetUI        #
###################

func planet_ui_show() -> void:
	var sector : Sector = sector_get_current()
	assert sector != null
	var planet : Planet = sector.planet_nearest($Player/PlayerSpaceship.get_position())
	if planet.is_scanned:
		planet_ui.visible = true
		planet_ui.reset($Player, planet)
		inventory_ui.sell_enable(planet)
		
func planet_ui_hide() -> void:
	planet_ui.visible = false
	inventory_ui.sell_disable()
	
###########
# PauseUI #
###########

func pause_ui_show() -> void:
	if $Player/PlayerSpaceship.get_state() == "ControlableState":
		$PauseUI/PauseMenu.visible = true
		get_tree().paused = true
	
func pause_ui_hide() -> void:
	$PauseUI/PauseMenu.visible = false

func _on_PauseButton_pressed():
	pause_ui_show()