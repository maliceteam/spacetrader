extends Node2D

var stars = []
onready var width : int = get_viewport_rect().size.x
onready var height : int = get_viewport_rect().size.y
var is_fast_travel : bool = false
var fast_travel_dir : Vector2 = Vector2(0, -1)

func _ready() -> void:
	for i in range(0, 40):
		stars.append( Vector2(rand_range(0, width), rand_range(0, height)) )

func _process(delta) -> void:
	update()

	
func _draw() -> void:
	if is_fast_travel:
		for star in stars:
			draw_line(star, star - (fast_travel_dir * 64), Color(1.0, 1.0, 1.0, 1.0), 1.0)
	else:
		for star in stars:
			draw_circle(star, 1.0, Color(1.0, 1.0, 1.0, 1.0))
		