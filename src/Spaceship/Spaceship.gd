extends KinematicBody2D

class_name Spaceship

const MAX_VEL : float = 512.0
const FAST_TRAVEL_VEL : float = MAX_VEL * 2.0
const ORBIT_VEL : float = MAX_VEL / 5.0
const SLOW_VEL : float = MAX_VEL / 5.0
const FAST_VEL : float = MAX_VEL

const ANGLE_VEL : float = PI

var angle : float = 0
var dir : Vector2 = Vector2()
var vel : float = MAX_VEL

const MAX_FUEL : float = 100.0
const FAST_TRAVEL_CONSO_FUEL : float = -20.0
const CONSO_FUEL : float = -1.0
var fuel : float = 3.0 * MAX_FUEL/4.0

func _ready() -> void:
	pass 

func _process(delta) -> void:
	rotation = angle 
	var dir_angle : float = angle - PI/2
	dir = Vector2( cos(dir_angle), sin(dir_angle) )

func _physics_process(delta) -> void:
	move_and_slide(dir * vel)
	
func move_angle(offset : float) -> void:
	angle += offset

func get_angle() -> float:
	return angle
	
func set_angle(a : float) -> void:
	angle = a
	
func get_position() -> Vector2:
	return global_position

func set_position(pos : Vector2) -> void:
	global_position = pos

func get_fuel_amount() -> float:
	return fuel
	
func add_fuel(value : float) -> void:
	fuel += value
	
	if fuel < 0.0:
		fuel = 0.0
	elif fuel > MAX_FUEL:
		fuel = MAX_FUEL