extends Node2D

class PlanetStock:
	var cost : float
	var amount : int

class_name Planet
const PLANETS_MIN_DISTANCE : float = 64.0
const MIN_ID : int = 1
const MAX_ID : int = 17
const MIN_PRICE_RATIO : float = 0.5
const MAX_PRICE_RATIO : float = 2.0

var name_id : int = 1
var id : int = 1
var planet_name : String = ""
var radius = 0
var resources : Dictionary = {}
var sell_prices : Dictionary = {}
var is_scanned : bool = false

func _ready() -> void:
	pass

func serialize() -> Dictionary:
	var dict : Dictionary = {}
	dict["ID"] = id
	dict["NameID"] = name_id
	dict["PositionX"] = get_position().x
	dict["PositionY"] = get_position().y
	dict["IsScanned"] = is_scanned
	dict["PlanetName"] = planet_name
	dict["Radius"] = radius
	dict["Resources"] = {}
	for r in resources.keys():
		dict["Resources"][r.resource_name] = {}
		dict["Resources"][r.resource_name]["Cost"] = resources[r].cost
		dict["Resources"][r.resource_name]["Amount"] = resources[r].amount
	
	dict["SellPrices"] = {}
	for sp in sell_prices.keys():
		dict["SellPrices"][sp] = sell_prices[sp]
		
	return dict

func unserialize(dict : Dictionary) -> void:
	id = dict["ID"]
	name_id = dict["NameID"]
	set_id(id)
	set_position(Vector2(dict["PositionX"], dict["PositionY"]))
	is_scanned = dict["IsScanned"]
	planet_name = dict["PlanetName"]
	radius = dict["Radius"]
	var dres : Dictionary = dict["Resources"]
	
	for key in dres.keys():
		var stock : PlanetStock = PlanetStock.new()
		var resource : SpaceResource = null
		for sr in $SpaceResource.get_children():
			if sr.resource_name == key:
				resource = sr
		assert resource != null
		stock.amount = int(dres[key]["Amount"])
		stock.cost = dres[key]["Cost"]
		resources[resource] = stock
		
	for sp in dict["SellPrices"].keys():
		sell_prices[sp] = dict["SellPrices"][sp]
		
func set_position(pos : Vector2) -> void:
	global_position = pos

func get_position() -> Vector2:
	return global_position

func set_planet_name(name : String) -> void:
	planet_name = name

func get_planet_name() -> String:
	return planet_name
	
func set_id(i : int) -> void:
	assert id_exists(i)
	id = i
	
	var num_str = str(id)
	if id < 10:
		num_str = "0" + num_str
	
	var texture : Texture
	texture = load("res://Planet/Sprites/planet_" + num_str + ".png")
	 
	radius = texture.get_width() + PLANETS_MIN_DISTANCE
	$Sprite.texture = texture

func set_name_id(i : int) -> void:
	name_id = i

func id_exists(i : int) -> bool:
	return i >= MIN_ID and i <= MAX_ID

func round_f(value : float, dec : int) -> float:
	return round(value * pow(10, dec)) / pow(10, dec)

func generate_resources() -> void:
	for res in $SpaceResource.get_children():
		var prob : float = rand_range(0, 1)
		
		if prob <= res.probability:
			var stock : PlanetStock = PlanetStock.new()
			stock.cost = res.cost * round_f(rand_range(MIN_PRICE_RATIO, MAX_PRICE_RATIO), 2)
			stock.amount = int(round(rand_range(1, 10)))
			resources[res] = stock
			sell_prices[res.resource_name] = stock.cost
		else:
			sell_prices[res.resource_name] = res.cost * round_f(rand_range(MIN_PRICE_RATIO, MAX_PRICE_RATIO), 2)