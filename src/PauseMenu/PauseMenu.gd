extends Control

signal on_save

func _ready() -> void:
	pass

func _on_ResumeButton_pressed() -> void:
	get_tree().paused = false
	visible = false

func _on_SaveGameButton_pressed() -> void:
	emit_signal("on_save")
	get_tree().paused = false
	visible = false

func _on_MenuGameButton_pressed() -> void:
	get_tree().paused = false
	#get_tree().root.add_child(preload("res://Menu/Menu.tscn").instance())
	get_tree().root.add_child(preload("res://Menu/Menu.tscn").instance())
	
	for c in get_tree().root.get_children():
		if c.get_path() == "/root/Universe":
			get_tree().root.remove_child(c)
	
func _on_QuitButton_pressed() -> void:
	get_tree().quit()
