extends Control

signal on_reload_fuel
signal on_sell

var can_sell : bool = false
var planet : Planet = null
var inventory : Inventory = null

func _ready() -> void:
	pass

#warning-ignore:unused_argument
func _process(delta) -> void:
	if Input.is_action_just_pressed("reload_fuel"):
		reload_fuel()
	if inventory != null:
		$Panel/VBoxContainer/HSplitContainer/Label.text = "INVENTORY [" + str(inventory.capacity) + "/" + str(inventory.MAX_CAPACITY) + "]"
		$Panel/VBoxContainer/CapacityProgressBar.max_value = inventory.MAX_CAPACITY
		$Panel/VBoxContainer/CapacityProgressBar.value = inventory.capacity

func reset_resources() -> void:
	for child in $Panel/VBoxContainer/ResourcesVBoxContainer.get_children():
		$Panel/VBoxContainer/ResourcesVBoxContainer.remove_child(child)

func add_resources(resources : Dictionary) -> void:
	for res in resources.keys():
		if resources[res].amount > 0:
			
			var label : Label = Label.new()
			
			if can_sell:
				label.text = resources[res].resource.resource_name + " (" + str(planet.sell_prices[resources[res].resource.resource_name]) + "$) " + " x" + str(resources[res].amount)
			else:
				label.text = resources[res].resource.resource_name + " x" + str(resources[res].amount)
			
			var hbox : HBoxContainer = HBoxContainer.new()
			hbox.add_child(label)
			if can_sell:
				var button : Button = Button.new()
				button.text = "Sell"
#warning-ignore:return_value_discarded
				button.connect("pressed", self, "_on_sell_pressed", [resources[res].resource])
				hbox.add_child(button)
			$Panel/VBoxContainer/ResourcesVBoxContainer.add_child(hbox)
			
func _on_ReturnButton_pressed() -> void:
	visible = false
	pass
	
func _reset() -> void:
	reset_resources()
	add_resources(inventory.resources)

func _on_FuelButton_pressed() -> void:
	reload_fuel()

func reload_fuel() -> void:
	emit_signal("on_reload_fuel")

func _on_sell_pressed(resource) -> void:
	emit_signal("on_sell", resource, planet)
	_reset()
	
func sell_enable(p : Planet) -> void:
	planet = p
	can_sell = true
	_reset()
	
func sell_disable() -> void:
	planet = null
	can_sell = false
	_reset()