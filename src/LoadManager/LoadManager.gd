extends Node

func _ready() -> void:
	pass 

func change_scene(scene : Node) -> void:
	for c in get_tree().root.get_children():
		if c is Control:
			get_tree().root.remove_child(c)
	
	get_tree().root.add_child(scene)
	
func new_game() -> void:
	var loading : Node = preload("res://LoadingUI/LoadingUI.tscn").instance()
	change_scene(loading)
	
func load_game() -> void:
	
	var file : File = File.new()
	assert file.file_exists(SaveManager.SAVE_PATH)
	var err : int = file.open_encrypted_with_pass(SaveManager.SAVE_PATH, File.READ, SaveManager.SAVE_PASS)
	assert err == OK
	
	var universe : Node = preload("res://Universe/Universe.tscn").instance()
	
	# Player
	var player : Player = null
	for c in universe.get_children():
		if c.name == "Player":
			player = c
	
	assert player != null
	var player_dict : Dictionary = parse_json(file.get_line())
	player.unserialize(player_dict)
	
	# Universe
	var universe_dict : Dictionary = parse_json(file.get_line())
	universe.unserialize(universe_dict)

	file.close()
	
	change_scene(universe)